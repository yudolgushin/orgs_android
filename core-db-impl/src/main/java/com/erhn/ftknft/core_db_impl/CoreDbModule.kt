package com.erhn.ftknft.core_db_impl

import android.content.Context
import androidx.room.Room
import com.erhn.ftknft.core_db_api.db.AppDatabase
import com.erhn.ftknft.core_db_api.db.Db
import com.erhn.ftknft.core_db_api.db.dao.WeightsDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class CoreDbModule(val context: Context) {

    @Provides
    @Singleton
    fun provideDataBase(): AppDatabase = Room.databaseBuilder(context, AppDatabase::class.java, Db.NAME).build()

    @Provides
    @Singleton
    fun provideWeightsDao(appDatabase: AppDatabase): WeightsDao = appDatabase.weightsDao()
}