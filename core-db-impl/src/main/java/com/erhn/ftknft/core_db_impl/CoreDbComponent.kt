package com.erhn.ftknft.core_db_impl

import android.content.Context
import com.erhn.ftknft.core_db_api.CoreDbApi
import dagger.Component
import javax.inject.Singleton


@Component(
    modules = arrayOf(CoreDbModule::class)
)
@Singleton
interface CoreDbComponent : CoreDbApi {


    companion object {

        private var instance: CoreDbComponent? = null

        fun init(context: Context) {
            instance = DaggerCoreDbComponent.builder().coreDbModule(CoreDbModule(context)).build()
        }

        fun get(): CoreDbApi {
            return instance!!
        }
    }
}