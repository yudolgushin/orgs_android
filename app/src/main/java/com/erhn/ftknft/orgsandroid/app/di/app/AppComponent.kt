package com.erhn.ftknft.orgsandroid.app.di.app

import com.erhn.ftknft.orgsandroid.app.OrgsApp
import com.erhn.ftknft.orgsandroid.routing.MainRouter
import com.github.terrakok.cicerone.Cicerone
import dagger.Component
import javax.inject.Singleton

@Component(
    modules = arrayOf(AppModule::class)
)
@Singleton
interface AppComponent {

    fun getMainRouter(): MainRouter

    fun getCicerone():Cicerone<MainRouter>

    companion object {

        private var component: AppComponent? = null

        fun init(context: OrgsApp) {
            component?.let { throw RuntimeException("AppComponent already has been initialized") }
            synchronized(AppComponent::class) {
                component = DaggerAppComponent.builder()
                    .appModule(AppModule(context))
                    .build()
            }
        }

        fun get(): AppComponent {
            return component ?: throw RuntimeException("AppComponent must be initialized")
        }
    }
}