package com.erhn.ftknft.orgsandroid.app.di.activity

import com.erhn.ftknft.core_utils.di.PerScreen
import com.erhn.ftknft.orgsandroid.app.di.FeatureInjector
import com.erhn.ftknft.orgsandroid.presentation.presenter.MainPresenter
import com.erhn.ftknft.orgsandroid.routing.MainRouter
import dagger.Module
import dagger.Provides

@Module
class ActivityModule(val mainRouter: MainRouter) {

    @Provides
    @PerScreen
    fun provideMainPresenter(): MainPresenter = MainPresenter(mainRouter, FeatureInjector.featureAuth().authInteractor())
}