package com.erhn.ftknft.orgsandroid.app.di

import com.erhn.ftknft.core_network_impl.di.CoreNetworkComponent
import com.erhn.ftknft.feature_auth_api.FeatureAuthApi
import com.erhn.ftknft.feature_auth_impl.di.FeatureAuthComponent

object FeatureInjector {

    fun featureAuth(): FeatureAuthApi = FeatureAuthComponent.get(CoreNetworkComponent.get())
}