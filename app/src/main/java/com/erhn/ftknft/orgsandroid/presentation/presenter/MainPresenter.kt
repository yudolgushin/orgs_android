package com.erhn.ftknft.orgsandroid.presentation.presenter

import com.erhn.ftknft.core_ui.arch.mvp.BaseMvpPresenter
import com.erhn.ftknft.feature_auth_api.domain.AuthInteractor
import com.erhn.ftknft.orgsandroid.presentation.ui.MainView
import com.erhn.ftknft.orgsandroid.routing.MainRouter
import javax.inject.Inject

class MainPresenter @Inject constructor(private val mainRouter: MainRouter, private val authInteractor: AuthInteractor) :
    BaseMvpPresenter<MainView>() {

}