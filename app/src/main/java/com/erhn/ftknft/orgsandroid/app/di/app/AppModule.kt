package com.erhn.ftknft.orgsandroid.app.di.app

import android.content.Context
import com.erhn.ftknft.orgsandroid.app.OrgsApp
import com.erhn.ftknft.orgsandroid.routing.MainRouter
import com.github.terrakok.cicerone.Cicerone
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(val context: OrgsApp) {

    @Provides
    @Singleton
    fun provideContext(): Context = context

    @Provides
    @Singleton
    fun provideMainCicerone(): Cicerone<MainRouter> = Cicerone.create(MainRouter())


    @Provides
    @Singleton
    fun provideMainRouter(cicerone: Cicerone<MainRouter>): MainRouter = cicerone.router


}