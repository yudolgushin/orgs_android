package com.erhn.ftknft.orgsandroid.presentation.ui

import android.os.Bundle
import com.erhn.ftknft.core_ui.arch.mvp.MvpActivity
import com.erhn.ftknft.orgsandroid.R
import com.erhn.ftknft.orgsandroid.app.di.activity.ActivityComponent
import com.erhn.ftknft.orgsandroid.app.di.app.AppComponent
import com.erhn.ftknft.orgsandroid.presentation.presenter.MainPresenter
import com.erhn.ftknft.orgsandroid.routing.MainNavigator
import com.erhn.ftknft.orgsandroid.routing.MainRouter
import com.github.terrakok.cicerone.Cicerone

class MainActivity() : MvpActivity<MainView, MainPresenter>(R.layout.activity_main) {


    private val cicerone: Cicerone<MainRouter> = AppComponent.get().getCicerone()

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        presenter.onViewInit(savedInstanceState == null)
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        cicerone.getNavigatorHolder().setNavigator(MainNavigator(this, R.id.main_container))
    }

    override fun onPause() {
        super.onPause()
        cicerone.getNavigatorHolder().removeNavigator()
    }


    override fun onDestroy() {
        super.onDestroy()
        ActivityComponent.clear()
    }

    override fun initPresenter(): MainPresenter = ActivityComponent.get().getPresenter()
}