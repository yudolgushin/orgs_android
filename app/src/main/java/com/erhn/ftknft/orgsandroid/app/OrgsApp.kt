package com.erhn.ftknft.orgsandroid.app

import android.app.Application
import com.erhn.ftknft.core_db_impl.CoreDbComponent
import com.erhn.ftknft.core_network_impl.di.CoreNetworkComponent
import com.erhn.ftknft.core_utils.Logger
import com.erhn.ftknft.orgsandroid.BuildConfig
import com.erhn.ftknft.orgsandroid.app.di.app.AppComponent
import javax.inject.Inject

class OrgsApp @Inject constructor() : Application() {


    override fun onCreate() {
        super.onCreate()
        instance = this
        AppComponent.init(this)
        CoreNetworkComponent.init(this, BuildConfig.DEBUG)
        CoreDbComponent.init(this)
        Logger.initialize(BuildConfig.DEBUG)
    }

    companion object {
        lateinit var instance: OrgsApp
            private set
    }


}