package com.erhn.ftknft.orgsandroid.routing

import androidx.fragment.app.FragmentActivity
import com.github.terrakok.cicerone.androidx.AppNavigator

class MainNavigator(activity: FragmentActivity, containerId: Int) :
    AppNavigator(activity, containerId)