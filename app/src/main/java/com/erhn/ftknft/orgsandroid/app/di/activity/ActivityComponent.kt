package com.erhn.ftknft.orgsandroid.app.di.activity

import com.erhn.ftknft.core_utils.di.PerScreen
import com.erhn.ftknft.orgsandroid.app.di.app.AppComponent
import com.erhn.ftknft.orgsandroid.presentation.presenter.MainPresenter
import dagger.Component

@Component(
    modules = arrayOf(
        ActivityModule::class
    )
)
@PerScreen
interface ActivityComponent {

    fun getPresenter(): MainPresenter

    companion object {

        private var instance: ActivityComponent? = null

        fun get(): ActivityComponent {
            if (instance == null) {
                synchronized(ActivityComponent::class) {
                    if (instance == null) {
                        instance = DaggerActivityComponent.builder()
                            .activityModule(ActivityModule(AppComponent.get().getMainRouter()))
                            .build()
                    }
                }
            }
            return instance!!
        }

        fun clear() {
            instance = null
        }
    }
}