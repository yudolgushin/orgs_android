package com.erhn.ftknft.core_ui.arch.base

interface BackPressedable {
    fun onBackPressed() {}
}