package com.erhn.ftknft.core_ui.arch.mvp

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancel
import kotlin.coroutines.CoroutineContext

open class BaseMvpPresenter<V : MvpView> : Presenter<V>(), CoroutineScope {

    private val job = SupervisorJob()

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job


    open fun onViewInit(isEmptyBundle: Boolean) {}


    override fun onClean() {
        super.onClean()
        cancel()
    }
}