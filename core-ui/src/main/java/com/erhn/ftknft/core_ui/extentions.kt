package com.erhn.ftknft.core_ui

import android.content.Context
import android.graphics.Rect
import android.view.View
import android.view.ViewGroup
import androidx.core.graphics.Insets
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


fun View.measureDimension(desiredSize: Int, measureSpec: Int): Int {
    val size = View.MeasureSpec.getSize(measureSpec)
    val mode = View.MeasureSpec.getMode(measureSpec)
    return when (mode) {
        View.MeasureSpec.EXACTLY -> size
        View.MeasureSpec.AT_MOST -> Math.min(desiredSize, size)
        else -> desiredSize
    }
}

fun View.enable() {
    this.isEnabled = true
}

fun View.disable() {
    this.isEnabled = false
}

fun View.enable(isEnable: Boolean) {
    if (isEnable) {
        enable()
    } else {
        disable()
    }
}

fun View.show(isShow: Boolean) {
    if (isShow) {
        visibility = View.VISIBLE
    } else {
        visibility = View.INVISIBLE
    }
}

fun View.fadeShow(isShow: Boolean) {
    if (isShow) {
        visible()
        animate().alpha(1f).setDuration(300L)
    } else {
        animate().alpha(0f).setDuration(300L).withEndAction {
            invisible()
        }
    }
}

fun View.gone(isGone: Boolean) {
    if (isGone) {
        gone()
    } else {
        visible()
    }
}

fun View.fadeGone(isShow: Boolean) {
    if (!isShow) {
        visible()
        animate().alpha(1f).setDuration(300L)
    } else {
        animate().alpha(0f).setDuration(300L).withEndAction {
            gone()
        }
    }
}

fun View.gone() {
    visibility = View.GONE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.doOnApplyWindowInsets(block: (View, WindowInsetsCompat, Rect, Rect) -> WindowInsetsCompat) {

    val initialPadding = recordInitialPaddingForView(this)
    val initialMargins = recordInitialMarginForView(this)

    ViewCompat.setOnApplyWindowInsetsListener(this) { v, insets ->
        block(v, insets, initialPadding, initialMargins)
    }

    requestApplyInsetsWhenAttached()
}

private fun recordInitialPaddingForView(view: View) =
    Rect(view.paddingLeft, view.paddingTop, view.paddingRight, view.paddingBottom)

private fun recordInitialMarginForView(view: View): Rect {
    (view.layoutParams as? ViewGroup.MarginLayoutParams)?.let {
        return Rect(it.leftMargin, it.topMargin, it.rightMargin, it.bottomMargin)
    }
    return Rect()
}

fun WindowInsetsCompat.resume(
    l: Int = systemWindowInsetLeft, t: Int = systemWindowInsetTop, r: Int = systemWindowInsetRight, b: Int = systemWindowInsetBottom
) = WindowInsetsCompat.Builder()
    .setSystemWindowInsets(Insets.of(l, t, r, b))
    .build()

fun View.requestApplyInsetsWhenAttached() {
    if (isAttachedToWindow) {
        requestApplyInsets()
    } else {
        addOnAttachStateChangeListener(object : View.OnAttachStateChangeListener {
            override fun onViewAttachedToWindow(v: View) {
                v.removeOnAttachStateChangeListener(this)
                v.requestApplyInsets()
            }

            override fun onViewDetachedFromWindow(v: View) = Unit
        })
    }
}

fun Context.dpToPx(dp: Int): Float = resources.displayMetrics.density * dp

fun Context.dpToPxInt(dp: Int): Int = Math.round(resources.displayMetrics.density * dp)

suspend fun <T> withIOContext(block: suspend CoroutineScope.() -> T): T {
    return withContext(Dispatchers.IO, block)
}

suspend fun <T> withMainContext(block: suspend CoroutineScope.() -> T): T {
    return withContext(Dispatchers.Main, block)
}