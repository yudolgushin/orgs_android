package com.erhn.ftknft.orgsandroid.utils

inline fun <reified T : Any> simpleName(): String = T::class.java.simpleName

inline fun <reified T : Any> name(): String = T::class.java.name

inline fun <reified T : Any> canonicalName(): String? = T::class.java.canonicalName




