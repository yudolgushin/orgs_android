package com.erhn.ftknft.core_ui.arch.common

import android.os.Bundle
import androidx.fragment.app.Fragment
import java.util.*

abstract class SavingFragment(contentLayoutId: Int) : Fragment(contentLayoutId) {


    private lateinit var savingKey: String

    protected lateinit var savableEntity: SaveableEntity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savingKey = savedInstanceState?.getString(SAVING_KEY_FRAGMENT) ?: UUID.randomUUID().toString()
        savableEntity = Store.get(savingKey) ?: initSaveableEntity()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(SAVING_KEY_FRAGMENT, savingKey)
        Store.save(savingKey, savableEntity)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (!savableEntity.isSave) {
            savableEntity.clean()
        }
    }


    internal abstract fun initSaveableEntity(): SaveableEntity

    companion object {
        private const val SAVING_KEY_FRAGMENT = "saving_key_fragment"
    }
}