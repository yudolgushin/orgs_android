package com.erhn.ftknft.core_ui.widgets.orgschartview

class ChartPoints {

    val points = arrayListOf<ChartPoint>()

    fun getMax() = points.maxByOrNull { it.value }

    fun getMin() = if (isAllEquals()) null else
        points.minByOrNull { it.value }

    fun getMaxValue() = getMax()?.value ?: 0f

    fun getMinValue() = getMin()?.value ?: 0f

    fun getMaxMinDelta(): Float {
        val valueDelta = getMaxValue() - getMinValue()
        return if (valueDelta == 0f) return points.size.toFloat() else valueDelta
    }

    fun setPointRadius(radius: Float) {
        points.forEach { it.radius = radius }
    }

    fun pointsWidthSum(): Float {
        val diameter = points.firstOrNull()?.radius ?: 0 * 2F
        return diameter * points.size
    }

    fun setValues(values: List<Float>) {
        points.clear()
        points.addAll(values.map { ChartPoint().apply { value = it } })
    }

    private fun isAllEquals(): Boolean {
        var isAllEquals = true
        val firstElementValue = points.firstOrNull()?.value ?: 0f
        points.forEach {
            if (firstElementValue != it.value) {
                isAllEquals = false
            }
        }
        return isAllEquals
    }

    fun pointDiameter(): Float = points.firstOrNull()?.radius ?: 0 * 2f

    fun pointRadius(): Float = points.firstOrNull()?.radius ?: 0f


}