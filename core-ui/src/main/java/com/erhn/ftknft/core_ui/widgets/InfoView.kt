package com.erhn.ftknft.orgsandroid.presentation.widgets

import android.content.Context
import android.text.Spannable
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import androidx.annotation.StringRes
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatTextView
import com.erhn.ftknft.core_ui.R
import com.erhn.ftknft.core_ui.dpToPxInt

class InfoView : LinearLayout {

    private val messageTextView: AppCompatTextView
    private val actionButton: AppCompatButton

    private var onClick: ((View) -> Unit)? = null

    private val horizontalMargin = context.dpToPxInt(20)

    init {
        orientation = VERTICAL
        gravity = Gravity.CENTER
        inflate(context, R.layout.layout_info_view, this)
        messageTextView = findViewById(R.id.tv_message)
        actionButton = findViewById(R.id.btn_action)
        (actionButton.layoutParams as MarginLayoutParams).let {
            it.setMargins(
                horizontalMargin,
                it.topMargin,
                horizontalMargin,
                it.bottomMargin
            )
        }
        (messageTextView.layoutParams as MarginLayoutParams).let {
            it.setMargins(
                horizontalMargin,
                it.topMargin,
                horizontalMargin,
                it.bottomMargin
            )
        }
    }


    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        val tp = context.obtainStyledAttributes(attrs, R.styleable.InfoView)
        actionButton.text = tp.getText(R.styleable.InfoView_message)
        messageTextView.text = tp.getText(R.styleable.InfoView_actionText)
        tp.recycle()
    }

    fun setOnActionClick(onClick: (View) -> Unit) {
        this.onClick = onClick
        actionButton.setOnClickListener(onClick)
    }

    fun setActionButtonText(actionText: String) {
        actionButton.text = actionText
    }

    fun setActionButtonText(@StringRes textId: Int) {
        actionButton.text = resources.getText(textId)
    }

    fun setMessage(message: String) {
        messageTextView.text = message
    }

    fun setMessage(message: Spannable) {
        messageTextView.text = message
    }


    fun setMessage(@StringRes messageId: Int) {
        messageTextView.text = resources.getText(messageId)
    }
}