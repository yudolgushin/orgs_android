package com.erhn.ftknft.core_ui.widgets.orgschartview

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.os.Parcel
import android.os.Parcelable
import android.util.AttributeSet
import android.view.View
import com.erhn.ftknft.core_ui.R
import com.erhn.ftknft.core_ui.measureDimension
import com.erhn.ftknft.orgsandroid.presentation.widgets.orgschartview.IChartView

class ChartView : View, IChartView {

    private val density: Float = context.resources.displayMetrics.density

    private val scaledDensity: Float = context.resources.displayMetrics.scaledDensity

    private val charts = ChartPoints()

    private val pointsPaint = Paint()

    private val labelsPaint = Paint()

    private val linePaint = Paint()

    //sizes
    private var pointsRadius: Float = 3.dpFloat

    private var labelsTextSize: Float = 14.spFloat

    private var linesWidth: Float = 1.dpFloat

    //colors
    private var pointColor: Int = Color.BLACK

    private var lineColor: Int = Color.BLACK

    private var labelsColor: Int = Color.BLACK

    //labels
    private var maxLabel: ChartLabel = ChartLabel()

    private var minLabel: ChartLabel = ChartLabel()

    private val maxLabelBounds = Rect()

    private val minLabelBounds = Rect()

    constructor(context: Context?) : super(context) {
        initialize()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        val typeArray = context.obtainStyledAttributes(attrs, R.styleable.ChartView)
        pointColor = typeArray.getColor(R.styleable.ChartView_pointsColor, Color.BLACK)
        lineColor = typeArray.getColor(R.styleable.ChartView_linesColor, Color.BLACK)
        labelsColor = typeArray.getColor(R.styleable.ChartView_labelsColor, Color.BLACK)
        labelsTextSize = typeArray.getDimension(R.styleable.ChartView_labelsTextSize, 14.spFloat)
        linesWidth = typeArray.getDimension(R.styleable.ChartView_linesWidth, 1.dpFloat)
        pointsRadius = typeArray.getDimension(R.styleable.ChartView_pointsRadius, 3.dpFloat)
        typeArray.recycle()
        initialize()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val desiredWidth = paddingStart + paddingEnd + Math.round(charts.pointsWidthSum())
        val desiredHeight = paddingTop + paddingBottom + Math.round(charts.pointDiameter())
        val widthSize = measureDimension(desiredWidth, widthMeasureSpec)
        val heightSize = measureDimension(desiredHeight, heightMeasureSpec)

        val paddingText = Math.max(maxLabelBounds.width(), minLabelBounds.width())

        val startPointsPosition = paddingStart + charts.pointRadius() + paddingText / 2
        val endPointsPosition = widthSize - paddingEnd - paddingText / 2

        val topPointsPosition = paddingTop + charts.pointRadius() + (maxLabelBounds.height() * 2)
        val bottomPointsPosition = heightSize - paddingBottom - charts.pointRadius() - (minLabelBounds.height() * 2)

        val pointsAvailableHeight = bottomPointsPosition - topPointsPosition
        val pointsHeightStep = pointsAvailableHeight / charts.getMaxMinDelta()
        val pointsAvailableWidth = endPointsPosition - startPointsPosition
        val spaceBetweenPoints = (pointsAvailableWidth - charts.pointsWidthSum()) / (charts.points.size - 1)

        charts.points.forEachIndexed { index, chartPoint ->
            chartPoint.centerX = startPointsPosition + (spaceBetweenPoints + chartPoint.radius) * index
            chartPoint.centerY = topPointsPosition + (charts.getMaxValue() - chartPoint.value) * pointsHeightStep
            if (chartPoint == charts.getMax()) {
                maxLabel.x = chartPoint.centerX - maxLabelBounds.width() / 2f
                maxLabel.y = chartPoint.centerY - chartPoint.radius - (maxLabelBounds.height() / 2)
            } else if (chartPoint == charts.getMin()) {
                minLabel.x = chartPoint.centerX - minLabelBounds.width() / 2f
                minLabel.y = chartPoint.centerY + chartPoint.radius + minLabelBounds.height() * 1.5f
            }
        }

        setMeasuredDimension(widthSize, heightSize)
    }

    override fun onDraw(canvas: Canvas) {
        charts.points.forEachIndexed { i, chartPoint ->
            if (i < charts.points.lastIndex) {
                val nextChartPoint = charts.points[i + 1]
                canvas.drawLine(chartPoint.centerX, chartPoint.centerY, nextChartPoint.centerX, nextChartPoint.centerY, linePaint)
            }
            canvas.drawCircle(chartPoint.centerX, chartPoint.centerY, chartPoint.radius, pointsPaint)
        }
        canvas.drawText(maxLabel.text, maxLabel.x, maxLabel.y, labelsPaint)
        canvas.drawText(minLabel.text, minLabel.x, minLabel.y, labelsPaint)
    }

    override fun setValues(values: List<Float>) {
        charts.setValues(values)
        charts.setPointRadius(pointsRadius)
        invalidateTextMeasures()
        requestLayout()
    }

    override fun setTextSize(textSize: Float) {
        this.labelsTextSize = textSize
        pointsPaint.textSize = textSize
        invalidateTextMeasures()
        requestLayout()
    }

    override fun setPointRadius(radius: Float) {
        charts.setPointRadius(radius)
        requestLayout()
    }

    override fun setLineWidth(lineWidth: Float) {
        this.linesWidth = lineWidth
        pointsPaint.strokeWidth = this.linesWidth
        invalidate()
    }

    override fun setLineColor(color: Int) {
        lineColor = color
        linePaint.color = lineColor
        invalidate()
    }

    override fun setPointColor(color: Int) {
        pointColor = color
        pointsPaint.color = pointColor
        invalidate()
    }

    override fun setLabelColor(color: Int) {
        labelsColor = color
        labelsPaint.color = labelsColor
        invalidate()
    }

    override fun onSaveInstanceState(): Parcelable {
        val superStater: Parcelable? = super.onSaveInstanceState()
        val saveState = SaveState(superStater)
        saveState.values = ArrayList(charts.points.map { it.value })
        return saveState
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        if (state is SaveState) {
            setValues(state.values)
        }
        super.onRestoreInstanceState(state)
    }

    private fun initialize() {
        pointsPaint.apply {
            isAntiAlias = true
            color = pointColor
            style = Paint.Style.FILL_AND_STROKE
        }

        linePaint.apply {
            isAntiAlias = true
            color = lineColor
            strokeWidth = linesWidth
            style = Paint.Style.STROKE
            strokeCap = Paint.Cap.ROUND
        }

        labelsPaint.apply {
            isAntiAlias = true
            color = labelsColor
            style = Paint.Style.FILL_AND_STROKE
        }

        invalidateTextMeasures()
        charts.setPointRadius(pointsRadius)
    }

    private fun invalidateTextMeasures() {
        labelsPaint.textSize = labelsTextSize
        maxLabel.text = String.format("%.2f", charts.getMaxValue())
        minLabel.text = String.format("%.2f", charts.getMinValue())
        labelsPaint.getTextBounds(maxLabel.text, 0, maxLabel.text.length, maxLabelBounds)
        labelsPaint.getTextBounds(minLabel.text, 0, minLabel.text.length, minLabelBounds)
    }

    class SaveState : BaseSavedState {
        var values = ArrayList<Float>()

        constructor(source: Parcel?) : super(source) {
            values = source?.readSerializable() as ArrayList<Float>
        }

        constructor(superState: Parcelable?) : super(superState)

        override fun writeToParcel(out: Parcel?, flags: Int) {
            super.writeToParcel(out, flags)
            out?.writeSerializable(this.values)
        }

        companion object {

            val CREATOR = object : Parcelable.Creator<SaveState> {
                override fun createFromParcel(source: Parcel?): SaveState {
                    return SaveState(source)
                }

                override fun newArray(size: Int): Array<SaveState?> {
                    return arrayOfNulls(size)
                }
            }
        }
    }

    private val Int.dpFloat
        get() = this * density

    private val Int.dp
        get() = Math.round(this * density)

    private val Int.spFloat
        get() = scaledDensity * this

    private val Int.sp
        get() = Math.round(this * scaledDensity)

    private val Int.fromDpToPx: Float
        get() = this / density

}