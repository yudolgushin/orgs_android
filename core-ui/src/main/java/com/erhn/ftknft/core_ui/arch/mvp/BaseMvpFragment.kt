package com.erhn.ftknft.core_ui.arch.mvp

import android.os.Bundle
import android.view.View

abstract class BaseMvpFragment<V : MvpView, P : BaseMvpPresenter<V>>(contentLayoutId: Int) : MvpFragment<V, P>(contentLayoutId) {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.onViewInit(savedInstanceState == null)
    }

}