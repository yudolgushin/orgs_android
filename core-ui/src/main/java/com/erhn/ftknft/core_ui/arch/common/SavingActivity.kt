package com.erhn.ftknft.core_ui.arch.common

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import java.util.*

abstract class SavingActivity(contentLayoutId: Int) : AppCompatActivity(contentLayoutId) {

    private lateinit var savingKey: String

    protected lateinit var savableEntity: SaveableEntity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savingKey = savedInstanceState?.getString(SAVING_KEY_ACTIVITY) ?: UUID.randomUUID().toString()
        savableEntity = Store.get(savingKey) ?: initSaveableEntity()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(SAVING_KEY_ACTIVITY, savingKey)
        Store.save(savingKey, savableEntity)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (!savableEntity.isSave) {
            savableEntity.clean()
        }
    }

    internal abstract fun initSaveableEntity(): SaveableEntity

    companion object {
        private const val SAVING_KEY_ACTIVITY = "saving_key_activity"
    }
}