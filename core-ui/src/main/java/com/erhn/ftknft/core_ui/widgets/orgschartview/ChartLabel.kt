package com.erhn.ftknft.core_ui.widgets.orgschartview

class ChartLabel {
    var text: String = ""
    var x: Float = 0f
    var y: Float = 0f
}