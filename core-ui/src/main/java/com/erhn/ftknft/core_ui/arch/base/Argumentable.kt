package com.erhn.ftknft.core_ui.arch.base

import android.os.Bundle

interface Argumentable {
    fun args(): Bundle?
}