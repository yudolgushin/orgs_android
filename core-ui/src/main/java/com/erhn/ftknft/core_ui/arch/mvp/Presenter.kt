package com.erhn.ftknft.core_ui.arch.mvp

import com.erhn.ftknft.core_ui.arch.common.SaveableEntity

open class Presenter<V : MvpView> : SaveableEntity() {

    var view: V? = null
        private set

    fun attach(mvpView: MvpView) {
        view = mvpView as V
        onAttach()
    }

    fun detach() {
        onDetach()
        view = null
    }

    protected open fun onDetach() {}

    protected open fun onAttach() {}


}