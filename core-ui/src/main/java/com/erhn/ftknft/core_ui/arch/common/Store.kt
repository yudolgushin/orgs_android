package com.erhn.ftknft.core_ui.arch.common

internal object Store {

    private val entities = HashMap<String, SaveableEntity>()

    fun save(savingKey: String, entity: SaveableEntity) {
        entities.put(savingKey, entity)
        entity.isSave = true
    }

    fun get(key: String): SaveableEntity? = entities.remove(key)?.apply { isSave = false }
}