package com.erhn.ftknft.core_ui.arch.common

abstract class SaveableEntity {

    internal var isSave: Boolean = false

    internal fun clean() {
        onClean()
    }

    protected open fun onClean() {}
}