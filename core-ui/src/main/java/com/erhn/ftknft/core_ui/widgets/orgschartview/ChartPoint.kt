package com.erhn.ftknft.core_ui.widgets.orgschartview

import androidx.annotation.Px

class ChartPoint(var value: Float = 0f) {

    @Px
    var radius: Float = 0f

    @Px
    var centerX: Float = 0f

    @Px
    var centerY: Float = 0f


}