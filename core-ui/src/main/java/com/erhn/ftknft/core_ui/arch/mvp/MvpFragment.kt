package com.erhn.ftknft.core_ui.arch.mvp

import android.os.Bundle
import android.view.View
import com.erhn.ftknft.core_ui.arch.base.BaseFragment
import com.erhn.ftknft.core_ui.arch.common.SaveableEntity
import com.erhn.ftknft.core_ui.arch.common.SavingFragment

abstract class MvpFragment<V : MvpView, P : Presenter<V>>(contentLayoutId: Int) : SavingFragment(contentLayoutId), MvpView, BaseFragment {


    protected lateinit var presenter: P
        private set

    override fun args(): Bundle? = arguments

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = savableEntity as P
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attach(this)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detach()
    }

    final override fun initSaveableEntity(): SaveableEntity = initPresenter()

    abstract fun initPresenter(): P
}