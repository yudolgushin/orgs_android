package com.erhn.ftknft.core_ui.arch.mvp

import android.os.Bundle
import com.erhn.ftknft.core_ui.arch.common.SaveableEntity
import com.erhn.ftknft.core_ui.arch.common.SavingActivity

abstract class MvpActivity<V : MvpView, P : Presenter<V>>(contentLayoutId: Int) : SavingActivity(contentLayoutId), MvpView {

    protected lateinit var presenter: P
        private set

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = savableEntity as P
        presenter.attach(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detach()
    }

    abstract fun initPresenter(): P

    final override fun initSaveableEntity(): SaveableEntity = initPresenter()
}