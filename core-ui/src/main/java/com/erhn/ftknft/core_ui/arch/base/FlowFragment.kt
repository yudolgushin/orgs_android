package com.erhn.ftknft.core_ui.arch.base

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.erhn.ftknft.core_ui.R

class FlowFragment() : Fragment(R.layout.fragment_flow), BackPressedable {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onBackPressed() {
    }

}