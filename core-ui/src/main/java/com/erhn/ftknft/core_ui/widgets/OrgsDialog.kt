package com.erhn.ftknft.core_ui.widgets

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.annotation.StringRes
import androidx.core.view.updatePadding
import androidx.fragment.app.FragmentActivity
import com.erhn.ftknft.core_ui.databinding.DialogOrgsBinding
import com.erhn.ftknft.core_ui.doOnApplyWindowInsets
import com.erhn.ftknft.core_ui.dpToPxInt
import com.erhn.ftknft.core_ui.resume

class OrgsDialog private constructor(context: Context, val builder: Builder) : Dialog(context) {

    private val binding = DialogOrgsBinding.inflate(layoutInflater)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window?.setGravity(builder.gravity)
        setContentView(binding.root)
        setInsets()
        applyBuilderSettings()
    }

    private fun setInsets() {
        window?.decorView?.doOnApplyWindowInsets { v, i, paddings, margins ->
            v.updatePadding(
                i.systemWindowInsetLeft + paddings.left,
                i.systemWindowInsetTop + paddings.top,
                i.systemWindowInsetRight + paddings.right,
                i.systemWindowInsetBottom + paddings.bottom
            )
            return@doOnApplyWindowInsets i.resume()
        }
    }

    private fun applyBuilderSettings() {
        builder.apply {
            this@OrgsDialog.setCanceledOnTouchOutside(isCanceledOnOutSide)
            this@OrgsDialog.setCancelable(isCancelable)
            binding.apply {

                (root.layoutParams as? ViewGroup.MarginLayoutParams)?.apply {
                    setMargins(margins.left, margins.top, margins.right, margins.bottom)
                }

                if (title == null) {
                    tvTitle.visibility = View.GONE
                    spaceMessage.visibility = View.GONE
                } else {
                    tvTitle.text = title
                }

                if (message == null) {
                    tvMessage.visibility = View.GONE
                    spaceMessage.visibility = View.GONE
                } else {
                    tvMessage.text = message
                }

                if (positiveButtonText == null) {
                    btnPositive.visibility = View.GONE
                } else {
                    btnPositive.text = positiveButtonText
                    btnPositive.setOnClickListener { v ->
                        positiveButtonListener?.invoke(v)
                        dismiss()
                    }
                }

                if (negativeButtonText == null) {
                    btnNegative.visibility = View.GONE
                    btnNegative.visibility = View.GONE
                } else {
                    spacePositiveButton.visibility = View.VISIBLE
                    btnNegative.text = negativeButtonText
                    btnNegative.setOnClickListener { v ->
                        negativeButtonListener?.invoke(v)
                        dismiss()
                    }
                    if (positiveButtonText == null) {
                        spaceNegativeButton.visibility = View.GONE
                    }
                }
            }

        }
    }

    class Builder constructor(private val activity: FragmentActivity) {

        var title: String? = null
            private set

        var message: String? = null
            private set

        var positiveButtonText: String? = null
            private set

        var negativeButtonText: String? = null
            private set

        var positiveButtonListener: ((View) -> Unit)? = null
            private set

        var negativeButtonListener: ((View) -> Unit)? = null
            private set

        var isCanceledOnOutSide: Boolean = true
            private set

        var isCancelable: Boolean = true
            private set

        var gravity: Int = Gravity.CENTER
            private set

        var margins = Rect()

        private val defaultMargin = activity.dpToPxInt(16)

        fun setTitle(title: String): Builder {
            this.title = title
            return this
        }

        fun setTitle(@StringRes title: Int): Builder {
            this.title = activity.getString(title)
            return this
        }

        fun setMessage(message: String): Builder {
            this.message = message
            return this
        }

        fun setMessage(@StringRes message: Int): Builder {
            this.message = activity.getString(message)
            return this
        }

        fun setPositiveButton(text: String, listener: ((View) -> Unit)? = null): Builder {
            positiveButtonText = text
            positiveButtonListener = listener
            return this
        }

        fun setPositiveButton(@StringRes text: Int, listener: ((View) -> Unit)? = null): Builder {
            positiveButtonText = activity.getString(text)
            positiveButtonListener = listener
            return this
        }

        fun setNegativeButton(text: String, listener: ((View) -> Unit)? = null): Builder {
            negativeButtonText = text
            negativeButtonListener = listener
            return this
        }

        fun setNegativeButton(@StringRes text: Int, listener: ((View) -> Unit)? = null): Builder {
            negativeButtonText = activity.getString(text)
            negativeButtonListener = listener
            return this
        }

        fun setOutsideCanceled(isCanceledOnOutSide: Boolean): Builder {
            this.isCanceledOnOutSide = isCanceledOnOutSide
            return this
        }

        fun setCancelable(isCancelable: Boolean): Builder {
            this.isCancelable = isCancelable
            return this
        }

        fun setGravity(gravity: Int): Builder {
            this.gravity = gravity
            when (gravity) {
                Gravity.BOTTOM -> margins.bottom = defaultMargin
                Gravity.TOP -> margins.top = defaultMargin
                Gravity.START -> margins.left = defaultMargin
                Gravity.END -> margins.right = defaultMargin
            }
            return this
        }

        fun setMargins(left: Int = 0, top: Int = 0, right: Int = 0, bottom: Int = 0): Builder {
            margins.left = left
            margins.top = top
            margins.right = right
            margins.bottom = bottom
            return this
        }

        fun build(): OrgsDialog = OrgsDialog(activity, this)
    }
}