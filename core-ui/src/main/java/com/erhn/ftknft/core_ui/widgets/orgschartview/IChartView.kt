package com.erhn.ftknft.orgsandroid.presentation.widgets.orgschartview

import androidx.annotation.ColorInt
import androidx.annotation.Px

interface IChartView {

    fun setValues(values: List<Float>)

    fun setTextSize(@Px textSize: Float)

    fun setPointRadius(@Px radius: Float)

    fun setLineWidth(@Px lineWidth: Float)

    fun setLineColor(@ColorInt color: Int)

    fun setPointColor(@ColorInt color: Int)

    fun setLabelColor(@ColorInt color: Int)


}