package com.erhn.ftknft.orgsandroid.presentation.widgets

import android.content.Context
import android.graphics.Color
import android.text.Spannable
import android.util.AttributeSet
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageButton
import android.widget.ProgressBar
import android.widget.TextView
import androidx.annotation.StringRes
import com.erhn.ftknft.core_ui.R
import com.erhn.ftknft.core_ui.gone
import com.erhn.ftknft.core_ui.show
import com.erhn.ftknft.core_ui.visible

class OrgsToolbar : FrameLayout {

    val titleTextView: TextView
    val subtitleTextView: TextView
    val backButton: ImageButton
    val progress: ProgressBar
    private var currentTitle = ""

    private var onBackButtonClick: ((ImageButton) -> Unit)? = null

    init {
        inflate(context, R.layout.layout_toolbar, this)
        val density = context.resources.displayMetrics.density
        layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Math.round(density * 56))
        elevation = 4 * density
        setBackgroundColor(Color.WHITE)
        titleTextView = findViewById(R.id.tv_title)
        subtitleTextView = findViewById(R.id.tv_subtitle)
        backButton = findViewById(R.id.ib_back)
        progress = findViewById(R.id.proggress)
        backButton.setOnClickListener {
            onBackButtonClick?.invoke(backButton)
        }
        handleNavigationButton()
        handleSubtitle()
    }

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        val ta = context.obtainStyledAttributes(attrs, R.styleable.OrgsToolbar)
        titleTextView.text = ta.getString(R.styleable.OrgsToolbar_title)
        subtitleTextView.text = ta.getString(R.styleable.OrgsToolbar_subtitle)
        currentTitle = titleTextView.text.toString()
        handleSubtitle()
        handleNavigationButton()
        ta.recycle()
    }

    fun setTitle(text: String) {
        titleTextView.text = text
        currentTitle = text
        handleSubtitle()
        handleNavigationButton()
    }

    fun setTitle(text: Spannable) {
        titleTextView.text = text
        currentTitle = text.toString()
        handleSubtitle()
        handleNavigationButton()
    }


    fun setTitle(@StringRes textId: Int) {
        val string = context.getString(textId)
        titleTextView.text = string
        currentTitle = string
        handleSubtitle()
        handleNavigationButton()
    }

    fun setSubtitle(text: String) {
        subtitleTextView.text = text
        handleSubtitle()
        handleNavigationButton()
    }

    fun setSubtitle(spannable: Spannable) {
        subtitleTextView.text = spannable
        handleSubtitle()
        handleNavigationButton()
    }

    fun setSubtitle(@StringRes textId: Int) {
        subtitleTextView.text = context.getString(textId)
        handleSubtitle()
        handleNavigationButton()
    }

    fun setNavigationClick(onNavigationClick: ((ImageButton) -> Unit)?) {
        onBackButtonClick = onNavigationClick
        handleSubtitle()
        handleNavigationButton()
    }

    fun showProgress(isShow: Boolean) {
        progress.show(isShow)
        if (isShow) {
            titleTextView.text = context.getString(R.string.loading)
        } else {
            titleTextView.text = currentTitle
        }
    }

    private fun handleSubtitle() {
        if (subtitleTextView.text.toString().isEmpty()) {
            subtitleTextView.gone()
        } else {
            subtitleTextView.visible()
        }
    }

    private fun handleNavigationButton() {
        if (onBackButtonClick == null) {
            backButton.gone()
        } else {
            backButton.visible()
        }
    }

}