package com.erhn.ftknft.core_network_impl.di

import android.content.Context
import com.erhn.ftknft.core_network_api.CoreNetworkApi
import dagger.Component
import javax.inject.Singleton

@Component(
    modules = arrayOf(
        UserPrefsModule::class,
        InterceptorsModule::class,
        ClientsModule::class,
        ApiModule::class
    )
)
@Singleton
interface CoreNetworkComponent : CoreNetworkApi {

    companion object {

        private var component: CoreNetworkComponent? = null

        fun init(context: Context, isLogging: Boolean) {
            component = DaggerCoreNetworkComponent.builder()
                .userPrefsModule(UserPrefsModule(context))
                .clientsModule(ClientsModule(isLogging))
                .build()
        }

        fun get(): CoreNetworkComponent {
            return component!!
        }
    }
}