package com.erhn.ftknft.core_network_impl.di

import android.content.Context
import com.erhn.ftknft.core_network_api.prefs.AuthPrefs
import com.erhn.ftknft.core_network_impl.prefs.AuthPrefsImp
import dagger.Module
import dagger.Provides

@Module
class UserPrefsModule(val context: Context) {


    @Provides
    fun provideUsePrefs(): AuthPrefs = AuthPrefsImp(context)

}