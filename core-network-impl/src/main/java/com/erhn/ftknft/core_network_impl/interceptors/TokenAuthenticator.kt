package com.erhn.ftknft.core_network_impl.interceptors

import com.erhn.ftknft.core_network_api.api.RefreshApi
import com.erhn.ftknft.core_network_api.consts.Server
import com.erhn.ftknft.core_network_api.prefs.AuthPrefs
import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route

class TokenAuthenticator(private val authPrefs: AuthPrefs, private val refreshApi: RefreshApi) : Authenticator {
    override fun authenticate(route: Route?, response: Response): Request? {
        val tokens = refreshApi.refresh().execute().body()?.data
        return tokens?.let {
            authPrefs.saveUserCredentials(tokens.accessToken, tokens.refreshToken)
            response.request.newBuilder()
                .removeHeader(Server.HEADER.AUTHORIZATION)
                .addHeader(Server.HEADER.AUTHORIZATION, tokens.accessToken)
                .build()
        }
    }
}