package com.erhn.ftknft.core_network_impl.api.weights

import com.erhn.ftknft.core_network_api.api.WeightsApi
import com.erhn.ftknft.core_network_api.consts.Server
import com.erhn.ftknft.core_network_api.model.remote.WeightRemote
import com.erhn.ftknft.core_network_api.model.request.CreateWeightRequest
import com.erhn.ftknft.core_network_api.model.response.BaseResponse
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class WeightApiImp(authorizationClient: OkHttpClient) : WeightsApi {
    val api: WeightsApi

    init {
        api = Retrofit.Builder()
            .baseUrl(Server.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(authorizationClient)
            .build().create(WeightsApi::class.java)
    }

    override suspend fun getWeights(startDate: Long?, endDate: Long?, limit: Long?, page: Long?): BaseResponse<List<WeightRemote>> =
        api.getWeights(startDate, endDate, limit, page)

    override suspend fun createWeight(body: CreateWeightRequest): BaseResponse<WeightRemote> = api.createWeight(body)

    override suspend fun deleteWeight(weightId: Long): BaseResponse<Unit> = api.deleteWeight(weightId)
}