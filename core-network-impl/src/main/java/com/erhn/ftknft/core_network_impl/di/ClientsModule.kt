package com.erhn.ftknft.core_network_impl.di

import com.erhn.ftknft.core_network_api.interceptors.AuthorizationHeaderInterceptor
import com.erhn.ftknft.core_network_api.interceptors.RefreshHeaderInterceptor
import com.erhn.ftknft.core_network_impl.Client
import dagger.Module
import dagger.Provides
import okhttp3.Authenticator
import okhttp3.OkHttpClient
import javax.inject.Named
import javax.inject.Singleton

@Module
class ClientsModule(val isLogging: Boolean) {

    @Provides
    @Singleton
    @Named(Client.LOGGING)
    fun provideLoggingClient(): OkHttpClient = Client.logging(isLogging)

    @Provides
    @Singleton
    @Named(Client.AUTHORIZATION)
    fun provideAuthorization(authInterceptor: AuthorizationHeaderInterceptor, authenticator: Authenticator): OkHttpClient =
        Client.authorization(authInterceptor, authenticator, isLogging)

    @Provides
    @Singleton
    @Named(Client.REFRESH)
    fun provideRefresh(refreshInterceptor: RefreshHeaderInterceptor): OkHttpClient = Client.refresh(refreshInterceptor, isLogging)
}