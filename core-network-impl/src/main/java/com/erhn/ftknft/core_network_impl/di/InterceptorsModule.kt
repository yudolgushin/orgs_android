package com.erhn.ftknft.core_network_impl.di

import com.erhn.ftknft.core_network_api.api.RefreshApi
import com.erhn.ftknft.core_network_api.interceptors.AuthorizationHeaderInterceptor
import com.erhn.ftknft.core_network_api.interceptors.RefreshHeaderInterceptor
import com.erhn.ftknft.core_network_api.prefs.AuthPrefs
import com.erhn.ftknft.core_network_impl.interceptors.TokenAuthenticator
import com.erhn.ftknft.core_network_impl.interceptors.authorization.AuthorizationHeaderInterceptorImp
import com.erhn.ftknft.core_network_impl.interceptors.refresh.RefreshHeaderInterceptorImp
import dagger.Module
import dagger.Provides
import okhttp3.Authenticator
import javax.inject.Singleton

@Module
class InterceptorsModule {

    @Provides
    @Singleton
    fun provideAuthorizationHeaderInterceptor(authPrefs: AuthPrefs): AuthorizationHeaderInterceptor =
        AuthorizationHeaderInterceptorImp(authPrefs)

    @Provides
    @Singleton
    fun provideRefreshInterceptor(authPrefs: AuthPrefs): RefreshHeaderInterceptor =
        RefreshHeaderInterceptorImp(authPrefs)

    @Provides
    @Singleton
    fun provideAuthenticator(authPrefs: AuthPrefs, refreshApi: RefreshApi): Authenticator = TokenAuthenticator(authPrefs, refreshApi)

}