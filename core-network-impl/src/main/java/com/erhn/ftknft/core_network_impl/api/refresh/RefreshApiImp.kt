package com.erhn.ftknft.core_network_impl.api.refresh

import com.erhn.ftknft.core_network_api.api.RefreshApi
import com.erhn.ftknft.core_network_api.consts.Server
import com.erhn.ftknft.core_network_api.model.remote.TokensRemote
import com.erhn.ftknft.core_network_api.model.response.BaseResponse
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RefreshApiImp(refreshClient: OkHttpClient) : RefreshApi {
    val api: RefreshApi

    init {
        api = Retrofit.Builder()
            .baseUrl(Server.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(refreshClient)
            .build().create(RefreshApi::class.java)
    }

    override fun refresh(): Call<BaseResponse<TokensRemote>> = api.refresh()
}