package com.erhn.ftknft.core_network_impl

import com.erhn.ftknft.core_network_api.interceptors.AuthorizationHeaderInterceptor
import com.erhn.ftknft.core_network_api.interceptors.RefreshHeaderInterceptor
import okhttp3.Authenticator
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor

object Client {

    fun logging(isLogging: Boolean = true): OkHttpClient = OkHttpClient.Builder()
        .addLogging()
        .build()


    fun authorization(
        authInterceptor: AuthorizationHeaderInterceptor,
        authenticator: Authenticator,
        isLogging: Boolean = true
    ): OkHttpClient = OkHttpClient.Builder()
        .addLogging()
        .addInterceptor(authInterceptor)
        .authenticator(authenticator)
        .build()

    fun refresh(refreshInterceptor: RefreshHeaderInterceptor, isLogging: Boolean = true): OkHttpClient = OkHttpClient.Builder()
        .addLogging()
        .addInterceptor(refreshInterceptor)
        .build()

    private fun OkHttpClient.Builder.addLogging(isLogging: Boolean = true): OkHttpClient.Builder {
        if (isLogging) {
            this.addInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY })
        }
        return this
    }

    const val LOGGING = "Logging"
    const val AUTHORIZATION = "Authorization"
    const val REFRESH = "Refresh"

}