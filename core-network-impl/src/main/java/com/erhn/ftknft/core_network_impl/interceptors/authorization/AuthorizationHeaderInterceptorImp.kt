package com.erhn.ftknft.core_network_impl.interceptors.authorization

import com.erhn.ftknft.core_network_api.consts.Server
import com.erhn.ftknft.core_network_api.interceptors.AuthorizationHeaderInterceptor
import com.erhn.ftknft.core_network_api.prefs.AuthPrefs
import okhttp3.Interceptor
import okhttp3.Response

class AuthorizationHeaderInterceptorImp(private val authPrefs: AuthPrefs) : AuthorizationHeaderInterceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val accessToken = authPrefs.accessToken()
        return if (accessToken != null) {
            val newRequest = chain.request().newBuilder()
                .addHeader(Server.HEADER.AUTHORIZATION, accessToken)
                .build()
            chain.proceed(newRequest)
        } else {
            chain.proceed(chain.request())
        }
    }

}