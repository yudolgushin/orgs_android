package com.erhn.ftknft.core_network_impl.interceptors.refresh

import com.erhn.ftknft.core_network_api.consts.Server
import com.erhn.ftknft.core_network_api.interceptors.RefreshHeaderInterceptor
import com.erhn.ftknft.core_network_api.prefs.AuthPrefs
import okhttp3.Interceptor
import okhttp3.Response

class RefreshHeaderInterceptorImp(private val authPrefs: AuthPrefs) : RefreshHeaderInterceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val refreshToken = authPrefs.refreshToken()
        return if (refreshToken != null) {
            val newRequest = chain.request().newBuilder()
                .addHeader(Server.HEADER.AUTHORIZATION, refreshToken)
                .build()
            chain.proceed(newRequest)
        } else {
            chain.proceed(chain.request())
        }
    }

}