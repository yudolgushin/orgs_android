package com.erhn.ftknft.core_network_impl.di

import com.erhn.ftknft.core_network_api.api.AuthApi
import com.erhn.ftknft.core_network_api.api.NotesApi
import com.erhn.ftknft.core_network_api.api.RefreshApi
import com.erhn.ftknft.core_network_api.api.WeightsApi
import com.erhn.ftknft.core_network_impl.Client
import com.erhn.ftknft.core_network_impl.api.auth.AuthApiImp
import com.erhn.ftknft.core_network_impl.api.notes.NotesApiImp
import com.erhn.ftknft.core_network_impl.api.refresh.RefreshApiImp
import com.erhn.ftknft.core_network_impl.api.weights.WeightApiImp
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import javax.inject.Named

@Module
class ApiModule {

    @Provides
    fun provideRefreshApi(@Named(Client.REFRESH) refreshClient: OkHttpClient): RefreshApi = RefreshApiImp(refreshClient)

    @Provides
    fun provideAuthApi(@Named(Client.LOGGING) loggingClient: OkHttpClient): AuthApi = AuthApiImp(loggingClient)

    @Provides
    fun provideWeightsApi(@Named(Client.AUTHORIZATION) authorizationClient: OkHttpClient): WeightsApi = WeightApiImp(authorizationClient)

    @Provides
    fun provideNotesApi(@Named(Client.AUTHORIZATION) authorizationClient: OkHttpClient): NotesApi = NotesApiImp(authorizationClient)


}