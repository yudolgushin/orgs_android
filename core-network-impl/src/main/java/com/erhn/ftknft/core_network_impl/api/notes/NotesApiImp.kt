package com.erhn.ftknft.core_network_impl.api.notes

import com.erhn.ftknft.core_network_api.api.NotesApi
import com.erhn.ftknft.core_network_api.consts.Server
import com.erhn.ftknft.core_network_api.model.remote.NoteRemote
import com.erhn.ftknft.core_network_api.model.request.CreateNoteRequest
import com.erhn.ftknft.core_network_api.model.response.BaseResponse
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class NotesApiImp(authClient: OkHttpClient) : NotesApi {

    private val api: NotesApi

    init {
        api = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(Server.BASE_URL)
            .client(authClient)
            .build().create(NotesApi::class.java)
    }

    override suspend fun createNote(reqBody: CreateNoteRequest): BaseResponse<Unit> =
        api.createNote(reqBody)

    override suspend fun getNotes(): BaseResponse<List<NoteRemote>> =
        api.getNotes()

    override suspend fun deleteNote(noteId: Long): BaseResponse<Unit> =
        api.deleteNote(noteId)

    override suspend fun updateNote(reqBody: CreateNoteRequest): BaseResponse<Unit> =
        api.updateNote(reqBody)
}