package com.erhn.ftknft.core_network_impl.api.auth

import com.erhn.ftknft.core_network_api.api.AuthApi
import com.erhn.ftknft.core_network_api.consts.Server
import com.erhn.ftknft.core_network_api.model.remote.TokensRemote
import com.erhn.ftknft.core_network_api.model.request.AuthRequest
import com.erhn.ftknft.core_network_api.model.response.BaseResponse
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class AuthApiImp(loggingClient: OkHttpClient) : AuthApi {

    private val api: AuthApi

    init {
        api = Retrofit.Builder()
            .baseUrl(Server.BASE_URL)
            .client(loggingClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(AuthApi::class.java)
    }


    override suspend fun auth(authReq: AuthRequest): BaseResponse<TokensRemote> = api.auth(authReq)

    override suspend fun registration(authReq: AuthRequest): BaseResponse<TokensRemote> = api.registration(authReq)

    override suspend fun checkLogin(login: String): BaseResponse<Unit> = api.checkLogin(login)
}