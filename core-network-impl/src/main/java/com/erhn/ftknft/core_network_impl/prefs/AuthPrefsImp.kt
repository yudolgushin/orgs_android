package com.erhn.ftknft.core_network_impl.prefs

import android.annotation.SuppressLint
import android.content.Context
import com.erhn.ftknft.core_network_api.prefs.AuthPrefs

class AuthPrefsImp(context: Context) : AuthPrefs {

    private val prefs = context.getSharedPreferences(TOKEN_PREFS, Context.MODE_PRIVATE)

    override fun saveUserCredentials(accessToken: String, refreshToken: String) {
        this.accessToken = accessToken
        this.refreshToken = refreshToken
        isAuth = true
    }

    override fun clearUserCredentials() {
        accessToken = null
        refreshToken = null
        isAuth = false
    }

    override fun accessToken(): String? {
        return accessToken
    }

    override fun refreshToken(): String? {
        return refreshToken
    }

    override fun isUserAuth(): Boolean {
        return isAuth
    }

    private var accessToken: String? = null
        get() = prefs.getString(ACCESS_TOKEN, null)
        @SuppressLint("ApplySharedPref")
        set(value) {
            prefs.edit().putString(ACCESS_TOKEN, value).commit()
            field = value
        }

    private var refreshToken: String? = null
        get() = prefs.getString(REFRESH_TOKEN, null)
        @SuppressLint("ApplySharedPref")
        set(value) {
            prefs.edit().putString(REFRESH_TOKEN, value).commit()
            field = value
        }

    private var isAuth: Boolean = false
        get() = prefs.getBoolean(IS_AUTH, false)
        @SuppressLint("ApplySharedPref")
        set(value) {
            prefs.edit().putBoolean(IS_AUTH, value).commit()
            field = value
        }

    companion object {
        private const val TOKEN_PREFS = "tokens_prefs"
        private const val ACCESS_TOKEN = "access_token"
        private const val REFRESH_TOKEN = "refresh_token"
        private const val IS_AUTH = "is_auth"
    }
}