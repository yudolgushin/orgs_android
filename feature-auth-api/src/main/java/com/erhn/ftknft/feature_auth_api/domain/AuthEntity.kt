package com.erhn.ftknft.feature_auth_api.domain

data class AuthEntity(
    val login: String,
    val password: String
)