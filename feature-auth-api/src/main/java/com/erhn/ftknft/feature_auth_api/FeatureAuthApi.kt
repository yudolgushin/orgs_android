package com.erhn.ftknft.feature_auth_api

import com.erhn.ftknft.feature_auth_api.domain.AuthInteractor

interface FeatureAuthApi {
    fun authInteractor(): AuthInteractor
}