package com.erhn.ftknft.feature_auth_api.domain

import com.erhn.ftknft.core_utils.result.Result

interface AuthInteractor {

    suspend fun auth(authEntity: AuthEntity): Result<Unit>

    suspend fun userIsAuth(): Boolean

    suspend fun registration(authEntity: AuthEntity): Result<Unit>

    suspend fun logout()
}