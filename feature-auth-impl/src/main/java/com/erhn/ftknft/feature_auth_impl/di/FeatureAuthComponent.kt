package com.erhn.ftknft.feature_auth_impl.di

import com.erhn.ftknft.core_network_api.CoreNetworkApi
import com.erhn.ftknft.feature_auth_api.FeatureAuthApi
import dagger.Component

@Component(
    modules = arrayOf(AuthModule::class),
    dependencies = arrayOf(CoreNetworkApi::class)
)
interface FeatureAuthComponent : FeatureAuthApi {


    companion object {
        private var component: FeatureAuthComponent? = null

        fun get(coreNetworkApi: CoreNetworkApi): FeatureAuthComponent {
            if (component == null) {
                synchronized(FeatureAuthComponent::class) {
                    if (component == null) {
                        component = DaggerFeatureAuthComponent.builder()
                            .coreNetworkApi(coreNetworkApi)
                            .build()
                    }
                }
            }

            return component!!
        }

    }

}