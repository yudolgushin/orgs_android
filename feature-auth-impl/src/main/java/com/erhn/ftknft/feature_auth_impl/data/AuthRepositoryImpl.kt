package com.erhn.ftknft.feature_auth_impl.data

import com.erhn.ftknft.core_network_api.api.AuthApi
import com.erhn.ftknft.core_network_api.model.request.AuthRequest
import com.erhn.ftknft.core_network_api.prefs.AuthPrefs
import com.erhn.ftknft.feature_auth_api.domain.AuthEntity
import com.erhn.ftknft.feature_auth_impl.domain.AuthRepository

class AuthRepositoryImpl(val authApi: AuthApi, val authPrefs: AuthPrefs) : AuthRepository {

    override suspend fun auth(authEntity: AuthEntity) {
        val response = authApi.auth(AuthRequest(authEntity.login, authEntity.password))
        authPrefs.saveUserCredentials(response.data.accessToken, response.data.refreshToken)
    }

    override suspend fun registration(authEntity: AuthEntity) {
        val response = authApi.registration(AuthRequest(authEntity.login, authEntity.password))
        authPrefs.saveUserCredentials(response.data.accessToken, response.data.refreshToken)
    }

    override suspend fun isUserAuth(): Boolean {
        return authPrefs.isUserAuth()
    }

    override suspend fun logoutUser() {
        authPrefs.clearUserCredentials()
    }
}