package com.erhn.ftknft.feature_auth_impl.domain

import com.erhn.ftknft.core_utils.result.Result
import com.erhn.ftknft.feature_auth_api.domain.AuthEntity
import com.erhn.ftknft.feature_auth_api.domain.AuthInteractor

class AuthInteractorImpl(val authRepository: AuthRepository) : AuthInteractor {

    override suspend fun auth(authEntity: AuthEntity): Result<Unit> =
        Result.execute { authRepository.auth(authEntity) }

    override suspend fun userIsAuth(): Boolean =
         authRepository.isUserAuth()

    override suspend fun registration(authEntity: AuthEntity): Result<Unit> =
        Result.execute { authRepository.registration(authEntity) }

    override suspend fun logout() {
        authRepository.logoutUser()
    }
}