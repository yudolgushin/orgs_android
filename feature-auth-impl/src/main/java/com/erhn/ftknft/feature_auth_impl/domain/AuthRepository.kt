package com.erhn.ftknft.feature_auth_impl.domain

import com.erhn.ftknft.feature_auth_api.domain.AuthEntity

interface AuthRepository {

    suspend fun auth(authEntity: AuthEntity)

    suspend fun registration(authEntity: AuthEntity)

    suspend fun isUserAuth(): Boolean

    suspend fun logoutUser()


}