package com.erhn.ftknft.feature_auth_impl.di

import com.erhn.ftknft.core_network_api.api.AuthApi
import com.erhn.ftknft.core_network_api.prefs.AuthPrefs
import com.erhn.ftknft.feature_auth_api.domain.AuthInteractor
import com.erhn.ftknft.feature_auth_impl.data.AuthRepositoryImpl
import com.erhn.ftknft.feature_auth_impl.domain.AuthInteractorImpl
import com.erhn.ftknft.feature_auth_impl.domain.AuthRepository
import dagger.Module
import dagger.Provides

@Module
class AuthModule {

    @Provides
    fun authRepository(authApi: AuthApi, authPrefs: AuthPrefs): AuthRepository = AuthRepositoryImpl(authApi, authPrefs)

    @Provides
    fun authInteractor(authRepository: AuthRepository): AuthInteractor = AuthInteractorImpl(authRepository)

}