package com.erhn.ftknft.feature_authorization_api

import com.github.terrakok.cicerone.androidx.AppScreen

interface FeatureAuthorizationApi {
    fun starterScreen(initialScreen: InitialScreen): AppScreen
}