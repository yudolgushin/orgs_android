package com.erhn.ftknft.core_utils

import android.util.Log

object Logger {

    private const val TAG = "ORGS_APP_TAG"

    private var isDisable: Boolean = false

    private var isInitialize: Boolean = false

    fun initialize(isEnableLog: Boolean) {
        isInitialize = true
        isDisable = !isEnableLog
    }

    fun log(msg: String) {
        printLog(TAG, msg)
    }

    fun log(tag: String, msg: String) {
        printLog(tag, msg)
    }

    fun log(tag: String, obj: Any?) {
        printLog(tag, obj.toString())
    }

    fun log(obj: Any?) {
        printLog(TAG, obj.toString())
    }

    fun add() {
        val methodName = Thread.currentThread().stackTrace[3].methodName
        val className = Thread.currentThread().stackTrace[3].className
        printLog(className, methodName)
    }

    fun add(obj: Any) {
        val methodName = Thread.currentThread().stackTrace[3].methodName
        val className = obj.javaClass.name
        printLog(className, methodName)
    }

    fun addMsg(msg: String) {
        val methodName = Thread.currentThread().stackTrace[3].methodName
        val className = Thread.currentThread().stackTrace[3].className
        printLog("$className: $methodName", msg)
    }


    fun error(tag: String, throwable: Throwable) {
        printErrorLog(tag, throwable.toString())
    }

    fun error(throwable: Throwable) {
        printErrorLog(TAG, throwable.toString())
    }

    fun addError(throwable: Throwable) {
        val methodName = Thread.currentThread().stackTrace[3].methodName
        val className = Thread.currentThread().stackTrace[3].className
        printErrorLog("$className: $methodName", throwable.toString())
    }

    private fun printLog(subTag: String, msg: String?) {
        if (isDisable || !isInitialize) return
        Log.d(TAG, "$subTag: ${msg}")
    }

    private fun printErrorLog(subTag: String, msg: String?) {
        if (isDisable || !isInitialize) return
        Log.e(TAG, "$subTag: ${msg}")
    }
}