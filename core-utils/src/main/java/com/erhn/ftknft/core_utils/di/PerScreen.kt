package com.erhn.ftknft.core_utils.di

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerScreen()
