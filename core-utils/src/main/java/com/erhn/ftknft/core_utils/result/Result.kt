package com.erhn.ftknft.core_utils.result

sealed class Result<out T> {

    class Success<out T>(val data: T) : Result<T>()

    class Failure(val exception: Throwable) : Result<Nothing>()

    fun isSuccess(): Boolean = this is Success

    fun isFailure(): Boolean = this is Failure

    fun asSuccess(): Success<T> = this as Success<T>

    fun asFailure(): Failure = this as Failure

    fun asSuccessOrNull(): Success<T>? = this as? Success<T>

    fun asFailureOrNull(): Failure? = this as? Failure

    fun asData(): T = (this as Success).data

    fun asException(): Throwable = (this as Failure).exception

    fun dataOrNull(): T? = (this as? Success)?.data

    inline fun handle(success: (data: T) -> Unit, error: (error: Throwable) -> Unit) {
        when (this) {
            is Success -> success(this.data)
            is Failure -> error(this.exception)
        }
    }

    inline fun onSuccess(action: (data: T) -> Unit) {
        if (this is Success) {
            action(this.data)
        }
    }

    inline fun onFailure(action: (exception: Throwable) -> Unit) {
        if (this is Failure) {
            action(this.exception)
        }
    }

    inline fun <F> andThen(action: (data: T) -> Result<F>): Result<F> {
        return when (this) {
            is Success -> action(data)
            is Failure -> this
        }
    }

    suspend fun <F> andThenExecute(action: suspend (data: T) -> Result<F>): Result<F> {
        return when (this) {
            is Success -> action(data)
            is Failure -> this
        }
    }


    companion object {

        inline fun <T> invoke(action: () -> T): Result<T> {
            return try {
                Success(action())
            } catch (e: Throwable) {
                Failure(e)
            }
        }

        suspend fun <T> execute(action: suspend () -> T): Result<T> {
            return try {
                Success(action())
            } catch (e: Throwable) {
                Failure(e)
            }
        }

        fun <T> success(data: T): Success<T> = Success(data)

        fun <T> failure(error: Exception): Failure = Failure(error)


    }
}