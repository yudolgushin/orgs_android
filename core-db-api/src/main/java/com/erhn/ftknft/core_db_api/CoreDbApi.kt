package com.erhn.ftknft.core_db_api

import com.erhn.ftknft.core_db_api.db.dao.WeightsDao

interface CoreDbApi {

    fun getWeightsDao(): WeightsDao
}