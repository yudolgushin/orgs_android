package com.erhn.ftknft.core_db_api.db

object Db {

    const val VERSION = 1
    const val NAME = "orgs_database"

    object Weights {
        const val TABLE_NAME = "weights"
        const val WEIGHT_ID = "id"
        const val WEIGHT_VALUE = "weight_value"
        const val WEIGHT_DATE = "weight_date"
    }

}