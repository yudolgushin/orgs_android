package com.erhn.ftknft.core_db_api.db.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.erhn.ftknft.core_db_api.db.Db

@Entity(tableName = Db.Weights.TABLE_NAME)
data class WeightLocal(
    @PrimaryKey
    @ColumnInfo(name = Db.Weights.WEIGHT_ID)
    val id: Long,
    @ColumnInfo(name = Db.Weights.WEIGHT_VALUE)
    val weightValue: Double,
    @ColumnInfo(name = Db.Weights.WEIGHT_DATE)
    val weightDate: Long
)