package com.erhn.ftknft.core_db_api.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.erhn.ftknft.core_db_api.db.dao.WeightsDao
import com.erhn.ftknft.core_db_api.db.model.WeightLocal

@Database(entities = arrayOf(WeightLocal::class), version = Db.VERSION)
abstract class AppDatabase : RoomDatabase() {

    abstract fun weightsDao(): WeightsDao

}