package com.erhn.ftknft.core_db_api.db.dao

import androidx.room.*
import com.erhn.ftknft.core_db_api.db.Db
import com.erhn.ftknft.core_db_api.db.model.WeightLocal

@Dao
interface WeightsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(weightLocal: WeightLocal)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(weights: List<WeightLocal>)

    @Delete
    suspend fun delete(weightLocal: WeightLocal)

    @Query("DELETE FROM ${Db.Weights.TABLE_NAME} WHERE ${Db.Weights.WEIGHT_ID} = :weightId")
    suspend fun deleteById(weightId: Long)

    @Delete
    suspend fun deleteAll(weights: List<WeightLocal>)

    @Query("DELETE FROM ${Db.Weights.TABLE_NAME}")
    suspend fun clear()

    @Query("SELECT * FROM ${Db.Weights.TABLE_NAME}")
    suspend fun getWeights(): List<WeightLocal>

    @Query("SELECT * FROM ${Db.Weights.TABLE_NAME} WHERE ${Db.Weights.WEIGHT_DATE} > :startDate AND ${Db.Weights.WEIGHT_DATE} < :endDate ORDER BY ${Db.Weights.WEIGHT_DATE} DESC LIMIT :limit")
    suspend fun getWeightsByFilter(startDate: Long, endDate: Long, limit: Long): List<WeightLocal>

}