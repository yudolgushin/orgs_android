package com.erhn.ftknft.core_network_api.api

import com.erhn.ftknft.core_network_api.consts.Server
import com.erhn.ftknft.core_network_api.model.remote.WeightRemote
import com.erhn.ftknft.core_network_api.model.request.CreateWeightRequest
import com.erhn.ftknft.core_network_api.model.response.BaseResponse
import retrofit2.http.*

interface WeightsApi {

    @GET(Server.PATH.WEIGHTS)
    suspend fun getWeights(
        @Query(Server.QUERY.START_DATE) startDate: Long? = null,
        @Query(Server.QUERY.END_DATE) endDate: Long? = null,
        @Query(Server.QUERY.LIMIT) limit: Long? = null,
        @Query(Server.QUERY.PAGE) page: Long? = null
    ): BaseResponse<List<WeightRemote>>

    @POST(Server.PATH.WEIGHTS)
    suspend fun createWeight(@Body body: CreateWeightRequest): BaseResponse<WeightRemote>

    @DELETE(Server.PATH.WEIGHTS_ID)
    suspend fun deleteWeight(@Path(Server.PATH.ID) weightId: Long): BaseResponse<Unit>

}