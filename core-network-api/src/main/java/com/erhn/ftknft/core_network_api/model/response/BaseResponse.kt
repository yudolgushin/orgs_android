package com.erhn.ftknft.core_network_api.model.response

import com.google.gson.annotations.SerializedName

data class BaseResponse<T>(
    @SerializedName("status")
    val status: Status,
    @SerializedName("data")
    val data: T
) {

    enum class Status {
        @SerializedName("success")
        SUCCESS,

        @SerializedName("error")
        ERROR,

        @SerializedName("unknown_error")
        UNKNOWN_ERROR
    }
}