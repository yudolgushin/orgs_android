package com.erhn.ftknft.core_network_api.consts

object Server {
    const val BASE_URL = "https://organize-weights.ddns.net"
    const val WEBSOCKET = "wss://organize-weights.ddns.net"


    object HEADER {
        const val AUTHORIZATION = "Authorization"
        const val CHECK_LOGIN = "Check-login"
    }


    object PATH {
        const val AUTH = "auth"
        const val REGISTRATION = "registration"
        const val CHECK_LOGIN = "check_login"
        const val REFRESH = "refresh"
        const val WEIGHTS = "weights"
        const val NOTES = "notes"
        const val NOTES_ID = "notes/{id}"
        const val WEIGHTS_ID = "weights/{id}"
        const val ID = "id"
    }

    object QUERY {
        const val START_DATE = "startDate"
        const val END_DATE = "endDate"
        const val PAGE = "page"
        const val LIMIT = "limit"
    }
}