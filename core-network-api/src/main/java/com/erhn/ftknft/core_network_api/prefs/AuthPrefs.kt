package com.erhn.ftknft.core_network_api.prefs

interface AuthPrefs {

    fun saveUserCredentials(accessToken: String, refreshToken: String)

    fun clearUserCredentials()

    fun accessToken(): String?

    fun refreshToken(): String?

    fun isUserAuth(): Boolean

}