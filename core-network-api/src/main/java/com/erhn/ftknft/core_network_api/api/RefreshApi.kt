package com.erhn.ftknft.core_network_api.api

import com.erhn.ftknft.core_network_api.consts.Server
import com.erhn.ftknft.core_network_api.model.remote.TokensRemote
import com.erhn.ftknft.core_network_api.model.response.BaseResponse
import retrofit2.Call
import retrofit2.http.GET

interface RefreshApi {
    @GET(Server.PATH.REFRESH)
    fun refresh(): Call<BaseResponse<TokensRemote>>
}