package com.erhn.ftknft.core_network_api.model.remote

import com.google.gson.annotations.SerializedName

data class WeightRemote(
    @SerializedName("id")
    val id: Long,
    @SerializedName("weight_value")
    val weightValue: Double,
    @SerializedName("weight_date")
    val weightDate: Long
)