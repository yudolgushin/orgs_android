package com.erhn.ftknft.core_network_api.model.request

import com.google.gson.annotations.SerializedName

data class CreateNoteRequest(
    @SerializedName("title")
    val title: String,
    @SerializedName("text")
    val text: String?
)