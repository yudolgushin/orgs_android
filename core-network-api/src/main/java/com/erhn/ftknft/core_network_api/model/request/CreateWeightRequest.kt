package com.erhn.ftknft.core_network_api.model.request

import com.google.gson.annotations.SerializedName

data class CreateWeightRequest(
    @SerializedName("weight_value")
    val weightValue: Double,
    @SerializedName("weight_date")
    val weightDate: Long? = null
)