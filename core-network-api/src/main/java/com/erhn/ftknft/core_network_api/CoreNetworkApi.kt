package com.erhn.ftknft.core_network_api

import com.erhn.ftknft.core_network_api.api.AuthApi
import com.erhn.ftknft.core_network_api.api.NotesApi
import com.erhn.ftknft.core_network_api.api.RefreshApi
import com.erhn.ftknft.core_network_api.api.WeightsApi
import com.erhn.ftknft.core_network_api.prefs.AuthPrefs

interface CoreNetworkApi {
    fun authApi(): AuthApi
    fun refreshApi(): RefreshApi
    fun notesApi(): NotesApi
    fun weightsApi(): WeightsApi
    fun authPrefs(): AuthPrefs
}