package com.erhn.ftknft.core_network_api.api

import com.erhn.ftknft.core_network_api.consts.Server
import com.erhn.ftknft.core_network_api.model.remote.TokensRemote
import com.erhn.ftknft.core_network_api.model.request.AuthRequest
import com.erhn.ftknft.core_network_api.model.response.BaseResponse
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

interface AuthApi {

    @POST(Server.PATH.AUTH)
    suspend fun auth(@Body authReq: AuthRequest): BaseResponse<TokensRemote>

    @POST(Server.PATH.REGISTRATION)
    suspend fun registration(@Body authReq: AuthRequest): BaseResponse<TokensRemote>

    @GET(Server.PATH.CHECK_LOGIN)
    suspend fun checkLogin(@Header(Server.HEADER.CHECK_LOGIN) login: String): BaseResponse<Unit>

}