package com.erhn.ftknft.core_network_api.model.remote

import com.google.gson.annotations.SerializedName

data class NoteRemote(
    @SerializedName("id")
    val id: Long,
    @SerializedName("title")
    val title: String,
    @SerializedName("text")
    val text: String,
    @SerializedName("create_date")
    val createDate: Long,
    @SerializedName("update_date")
    val updateDate: Long
)