package com.erhn.ftknft.core_network_api.api

import com.erhn.ftknft.core_network_api.consts.Server
import com.erhn.ftknft.core_network_api.model.remote.NoteRemote
import com.erhn.ftknft.core_network_api.model.request.CreateNoteRequest
import com.erhn.ftknft.core_network_api.model.response.BaseResponse
import retrofit2.http.*

interface NotesApi {

    @POST(Server.PATH.NOTES)
    suspend fun createNote(@Body reqBody: CreateNoteRequest): BaseResponse<Unit>

    @GET(Server.PATH.NOTES)
    suspend fun getNotes(): BaseResponse<List<NoteRemote>>

    @DELETE(Server.PATH.NOTES_ID)
    suspend fun deleteNote(@Path(Server.PATH.ID) noteId: Long): BaseResponse<Unit>

    @PUT(Server.PATH.NOTES_ID)
    suspend fun updateNote(@Body reqBody: CreateNoteRequest): BaseResponse<Unit>
}