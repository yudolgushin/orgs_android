package com.erhn.ftknft.core_network_api.model.remote

import com.google.gson.JsonElement
import com.google.gson.annotations.SerializedName

data class SocketMsg(
    @SerializedName("entity")
    val entity: String,
    @SerializedName("event_type")
    val eventType: String,
    @SerializedName("date")
    val date: Long,
    @SerializedName("data")
    val jsonData: JsonElement
)