package com.erhn.ftknft.core_network_api.interceptors

import okhttp3.Interceptor

interface AuthorizationHeaderInterceptor : Interceptor {
}