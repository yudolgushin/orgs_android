package com.erhn.ftknft.core_network_api.model.remote

import com.google.gson.annotations.SerializedName

data class TokensRemote(
    @SerializedName("access_token")
    val accessToken: String,
    @SerializedName("refresh_token")
    val refreshToken: String
)